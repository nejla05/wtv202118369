const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
const path = require('path');
const url = require('url');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.post('/', function (req, res) {
    let tijelo = req.body;
    let novaLinija = "\n" + tijelo['ime'] + "," + tijelo['prezime'] + "," + tijelo['adresa'] + "," + tijelo['broj_telefona'];
    fs.appendFile('imenik.csv', novaLinija, (err) => {
        if (err) throw err;
        console.log("Uspješno dodan novi red!");
        res.sendFile(path.join(__dirname, 'ispis.html'));
    });
});

app.get('/unos', (req, res) => {
    res.sendFile(path.join(__dirname, 'forma.html'));
});
app.post('/unos', (req, res) => {
    res.send("http://localhost:8085/unos");
});

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'ispis.html'));
});

app.get('/tabela', (req, res) => {
    fs.readFile('imenik.csv', (err, data) => {
        res.writeHead(200,{'Content-Type': 'application/json'});
        if (err) {     
            throw err;
        }
        let spisakLjudi = data.toString().split("\n");
        let niz = [];
        for (let i = 0; i < spisakLjudi.length; ++i) {
            let parametri = spisakLjudi[i].split(",");
            let objekat = {
                ime: parametri[0],
                prezime: parametri[1],
                adresa: parametri[2],
                broj_telefona: parametri[3]
            };
            niz.push(objekat);
        }    
        res.end(JSON.stringify(niz));
    });
});

app.listen(8085, () => {
    console.log("Uspjesno otvoren port 8085");
});