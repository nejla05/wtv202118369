const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
const path = require('path');
const url = require('url');
var mysql = require('mysql');

var connection = mysql.createConnection({       // Kreiranje konekcije
    host: "localhost",
    user: "root",
    password : "",
    database: "imenik"
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use('/css', express.static(__dirname));
app.set("view engine","pug");
app.set("views",path.join(__dirname,"views"));

connection.connect(function(err){
    if(err) throw err;
    console.log("Uspjesno konektovan");
})

app.get('/imenik',function(req,res){
    connection.query("SELECT * from imenik",function(err,result){
        if(err) throw err;
        res.render(path.join(__dirname, 'imenik.ejs'), {
            osobe: result
          });
    });
});

app.post('/imenik',(req,res) =>{
    connection.query("SELECT * from imenik",function(err,result){
        if(err) throw err;
        let tijelo_zahtjeva = req.body;

        let sql1 = `INSERT INTO imenik VALUES ('${tijelo_zahtjeva.ime}', '${tijelo_zahtjeva.prezime}', '${tijelo_zahtjeva.adresa}', '${tijelo_zahtjeva.broj_telefona}')`;

        connection.query("INSERT INTO imenik VALUES ('${tijelo_zahtjeva.ime}', '${tijelo_zahtjeva.prezime}', '${tijelo_zahtjeva.adresa}', '${tijelo_zahtjeva.broj_telefona}')",function(err,result){
            if(err) throw err;
            res.redirect('/imenik');
        });
    });
});

app.listen(3000,() =>{
    console.log("Konektovan na localhost 3000");
});