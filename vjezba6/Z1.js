const http = require('http');
const fs = require('fs');
 
http.createServer((zahtjev, odgovor) => {
    if(zahtjev.method == 'GET'){
        fs.readFile('imenik.csv', (err, data) => {  
            if (err) return console.error(err);
            
            let spisak = data.toString().split("\n"); // jedna osoba jedan red
            let niz = [];
            for(let i=0; i < spisak.length-1; i++){
                let parametri = spisak[i].split(",");
                let objekat = { 
                    Ime : parametri[0], 
                    prezime : parametri[1], 
                    adresa : parametri[2], 
                    broj_telefona : parametri[3] 
            };
        
                niz.push(objekat);
            }
            odgovor.end(JSON.stringify(niz));  // Konvertovanje objekta u string 
        });
    }
}).listen(8080);
