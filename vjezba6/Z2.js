const http = require('http');
const fs = require('fs');
const url = require('url'); 

http.createServer((zahtjev, odgovor) => {
    if(zahtjev.method == 'GET'){
        fs.readFile('imenik.csv', (err, data) => {  
            if (err) return console.error(err);
            let mojURL = new URL("localhost:8080" + zahtjev.url);
            let parametar = mojURL.searchParams.get('q');
            
            let spisak = data.toString().split("\n"); // jedna osoba jedan red
            let niz = [];

            if(parametar != null){
            for(let i=0; i < spisak.length-1; i++){
                let parametri = spisak[i].split(",");
                let objekat = { 
                    Ime : parametri[0], 
                    prezime : parametri[1], 
                    adresa : parametri[2], 
                    broj_telefona : parametri[3] 
            };
            if(objekat.Ime.contains(parametar)){
                niz.push(objekat);
            }
            }
        }
        else {
            for(let i=0; i < spisak.length-1; i++){
            let parametri = spisak[i].split(",");
            let objekat = { 
                Ime : parametri[0], 
                prezime : parametri[1], 
                adresa : parametri[2], 
                broj_telefona : parametri[3] 
        };
            niz.push(objekat);
        }
    }
            odgovor.end(JSON.stringify(niz));  // Konvertovanje objekta u string 
        });
    }
    else if(zahtjev.method == 'POST'){
        let tijeloZahtjeva = '';
        req.on('data',function(data){
            tijeloZahtjeva+=data;
        });
        req.on('end',function(){
            //primljen čitav zahtjev
            let parametri = new url.URLSearchParams(tijeloZahtjeva);
            let novaLinija = parametri.get('ime')+","+parametri.get('prezime')+
                ","+parametri.get('adresa')+","+parametri.get('broj_telefona');
            fs.appendFile('imenik.txt',novaLinija,function(err){
                if(err) throw err;
                console.log("Novi red uspješno dodan!");
                res.writeHead(200,{});
                res.end(parametri.toString());
            });
        });
    }
 }).listen(8080);
