function otvoriPrvuListu(){
    let el = document.getElementsByClassName('PrvaLista');
    if(el[0].style.display == "none"){
        el[0].style.display = "block";
        document.getElementById("prva_g").className = "minus";
    }
    else {
        el[0].style.display = "none";
        document.getElementById("prva_g").className = "prva";
    }
}

function otvoriDruguListu(){
    let el = document.getElementsByClassName('DrugaLista');
    if(el[0].style.display == "none"){
        el[0].style.display = "block";
        document.getElementById("druga_g").className = "minus";
    }
    else {
        el[0].style.display = "none";
        document.getElementById("druga_g").className = "druga";
    }
}